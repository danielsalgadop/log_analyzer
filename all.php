<?php
header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
header("Pragma: no-cache"); // HTTP 1.0.
header("Expires: 0"); // Proxies.
// 5- 
// will receive acces log file(s)
// will return a structure 
// day1=> "totals" [
//					status_code1 = "number",
//					status_code1_perc = "%",
//					status_code2 = "number",
//					status_code2_perc = "%",
//					status_code(n) = "number",
//					status_code(n)_perc = "%",
//					]
//		=> "hours" [
//			00 => [
//						status_code1 = "number",
//						status_code1_perc = "%",
//						status_code2 = "number",
//						status_code2_perc = "%",
//						status_code(n) = "number",
//						status_code(n)_perc = "%",
//					],
//			01 => [
//						status_code1 = "number",
//						status_code1_perc = "%",
//						status_code2 = "number",
//						status_code2_perc = "%",
//						status_code(n) = "number",
//						status_code(n)_perc = "%",
//					]
//			0(n) => [
//						status_code1 = "number",
//						status_code1_perc = "%",
//						status_code2 = "number",
//						status_code2_perc = "%",
//						status_code(n) = "number",
//						status_code(n)_perc = "%",
//					]
//		=> "top10" [
//				"ip1" => [
// 						"numbers" =>[
//							status_code1 = "number",
//							status_code1_perc = "%",
//							status_code2 = "number",
//							status_code2_perc = "%",
//							status_code(n) = "number",
//							status_code(n)_perc = "%",
//						],
// 						"petitions" => [
//							"petitionX1" => total,
//							"petitionX2" => total,
//							"petitionX(n)" => total,
//						]
//					]
//				"ip2" => as ip1
// day2=> as day1,
// day(n)=> as day1,










// $acces_log = file($argv[1]);
// print_r($acces_log);

$result = array();
// $tmp_path = "/tmp/log_analyzer";  // PROD
$tmp_path = "/tmp/log_analyzer_".time(); // DEV


if(!mkdir($tmp_path)){
	print "NO puedo crear ".$tmp_path."\n";
	exit;
}
// filter, get only petitions with "calculate" and separate day-by-day
// foreach (file($argv[1]) as $line) {
for ($i=1; $i < count($argv) ; $i++) { 
	$file = $argv[$i];
	# code...

	// recorrer los inputs desde terminal
	// foreach ($argv as  $value) {
	// print "ANALIZANDO $file \n";
	# code...

	// separate in day-by-day files
	$handle = fopen($file, "r");
	if ($handle) {
		while (($line = fgets($handle)) !== false) {
		    // process the line read.
			// list ($clientAddress,    $rfc1413,      $username, 
		  	//     $localTime,         $httpRequest,  $statusCode, 
		  	//     $bytesSentToClient, $referer,      $clientSoftware);
			preg_match('/^(\S+) (\S+) (\S+) \[(.+)\] \"(.+)\" (\S+) (\S+) \"(.*)\" \"(.*)\"/', $line,$matches);
			$http_request = $matches[5];
			if(!strpos($http_request, "payoutnetwork/calculate")) continue;
			// build file name
			preg_match('/(\d{2}\/\w{3,}\/\d{4}):(\d{2}):/',$matches[4],$localtime);
			$id = $localtime[1]; // dd/Mounth/YYYY
			$id = str_replace("/", "_", $id);
			file_put_contents($tmp_path."/".$id, $line,FILE_APPEND);
		}
		fclose($handle);
	} else {
	    print "error opening the file";
	    exit;
	}
// }
}
$all_results;
// read day-by-day and calculate
if ($gestor = opendir($tmp_path)) {
    /* Esta es la forma correcta de iterar sobre el directorio. */
    while (false !== ($file = readdir($gestor))) {
    	if ( preg_match('/^\./', $file) ) continue;

			
    	$one_day_result["totals"] = []; // se podria calcular con los "hours"  (TODO)
    	$one_day_result["top_10"] = 
    	[
    		"1" =>["ips"=>[],"total"=>0],
    		"2" =>["ips"=>[],"total"=>0],
    		"3" =>["ips"=>[],"total"=>0],
    		"4" =>["ips"=>[],"total"=>0],
    		"5" =>["ips"=>[],"total"=>0],
    		"6" =>["ips"=>[],"total"=>0],
    		"7" =>["ips"=>[],"total"=>0],
    		"8" =>["ips"=>[],"total"=>0],
    		"9" =>["ips"=>[],"total"=>0],
    		"10" =>["ips"=>[],"total"=>0],
    	];
    	$one_day_result["hours"] = 
    	[
    		"00"=>[],
    		"01"=>[],
    		"02"=>[],
    		"03"=>[],
    		"04"=>[],
    		"05"=>[],
    		"06"=>[],
    		"07"=>[],
    		"08"=>[],
    		"09"=>[],
    		"10"=>[],
    		"11"=>[],
    		"12"=>[],
    		"13"=>[],
    		"14"=>[],
    		"15"=>[],
    		"16"=>[],
    		"17"=>[],
    		"18"=>[],
    		"19"=>[],
    		"20"=>[],
    		"21"=>[],
    		"22"=>[],
    		"23"=>[]
    	];
 
 		$suspect_ips=[];
		$handle = fopen($tmp_path."/".$file, "r");
		if ($handle) {
			while (($line = fgets($handle)) !== false) {
				// print $line."\n\n";

			    // process the line read.
				// list ($clientAddress,    $rfc1413,      $username, 
			  	//     $localTime,         $httpRequest,  $statusCode, 
			  	//     $bytesSentToClient, $referer,      $clientSoftware);
				preg_match('/^(\S+) (\S+) (\S+) \[(.+)\] \"(.+)\" (\S+) (\S+) \"(.*)\" \"(.*)\"/', $line,$matches);
				list($all,$client_addres,$rfc1413,$username,$local_time,$http_request,$status_code,$bytes_sent_to_client,$referer,$client_software) = $matches;
				// print "client_addres [$client_addres] rfc1413 [$rfc1413] username [$username] local_time [$local_time] http_request [$http_request] status_code [$status_code] bytes_sent_to_client [$bytes_sent_to_client] referer [$referer] client_software [$client_software]\n";

				// rellenar [totals]
				if(!array_key_exists($status_code,$one_day_result["totals"])){
					$one_day_result["totals"][$status_code] = 0;
				}
				$one_day_result["totals"][$status_code]++;

				// rellenar [hour]
				preg_match('/\d{2}\/\w{3,}\/\d{4}:(\d{2}):/',$local_time,$localtime);
				$hour = $localtime[1]; // hh
				if(!array_key_exists($status_code, $one_day_result["hours"][$hour])){
					$one_day_result["hours"][$hour][$status_code] = 0;
				}
				$one_day_result["hours"][$hour][$status_code]++;

				// rellenar $suspect_ips
				if(!array_key_exists($client_addres, $suspect_ips)){
					$suspect_ips[$client_addres][$status_code] = 1;
				}
				else{ // new stauts
					if(!array_key_exists($status_code,$suspect_ips[$client_addres])){
						$suspect_ips[$client_addres][$status_code] = 1;
					}
				}
				$suspect_ips[$client_addres][$status_code]++;
			}
			fclose($handle);
		} else {
		    print "error opening the file";
		    exit;
		}



		$total_collector = calculateTotals($suspect_ips);
		krsort($total_collector);
		// print_r($total_collector);
		// exit;


		// print top 10 ip that are seen
		$i=1;
		foreach ($total_collector as $total_calls => $arr_ips) {
			if($i > 10) break;
			$one_day_result["top_10"][$i]["ips"] = $arr_ips;
			$one_day_result["top_10"][$i]["total"] = $total_calls;
			// $one_day_result["ZZZZZ"][$i]["total"] = 69;


			// $one_day_result["top_10"][$i]["ips"] = $arr_ips;
			// $one_day_result["top_10"][$i]["total"] = $totl_calls;
			$i++;
		}

		$all_results[$file] = $one_day_result;

    }
    closedir($gestor);
}



function calculateTotals($result){
	$total_collector = [];
	// recorrer totals
	foreach ($result as $ip => $array_status) {

		// print "ip [$ip]\n";
		$total_calls = 0;
		foreach ($array_status as $number) {
			$total_calls = $total_calls + $number;
		}
		if(!array_key_exists($total_calls, $total_collector)){ // primera vez que hay este resultado
			$total_collector[$total_calls] = [];
		}
		array_push($total_collector[$total_calls], $ip);
		// $top[$total_calls] = [$ip];
		// array_push($total_collector, "asdfd");
		// array_push(array, var)
	}
	return $total_collector;
}

function miHeader(){
	$html = <<<HTML
<html>
	<head>
		<title>Line Chart</title>
		<script src="vendor/Chart.min.js"></script>
		<!-- <script src="vendor/jQuery_v1.12.1.js"></script> -->
		<script>
		//function showDetails(id){
		//	$("#"+id).toggle();
		//}
		</script>
	</head>
	<body>
HTML;
print $html;
}
miHeader();

function tantoPorCiento($valor,$cien_por_cien){
	$valor = $valor * 100;
	return $valor / $cien_por_cien;
}

function generateCharts($all_results){

	// recorrer todos los dias
	foreach ($all_results as $ddmmyyyy => $arr_values) {
		print '<h1>'.$ddmmyyyy.'</h1>';
		// print '<div id="wrapper_'.$ddmmyyyy.'" style="display:none;">';  // display:none, o "visibility: hidden" provocan que no se calculen los charts

		// TOTALS
		print '<div>';
		$all_petitions_total = 0;
		foreach ($arr_values["totals"] as $status_code => $total) {
			print "STATUS CODE[".$status_code."] TOTAL [".$total."]<br>";
			$all_petitions_total+=$total;
		}
		print '</div>';

		// SUSPICIOUS IPS
		// print '<pre>';
		// print_r($arr_values["top_10"]);
		// print '</pre>';
		print '<div>';
		foreach ($arr_values["top_10"] as $arr_values) {
			print "NUMBER [".$arr_values["total"]."] PORCENTAJE [".tantoPorCiento($arr_values["total"],$all_petitions_total)."%] IP [".join(",",$arr_values["ips"])."]<br>";			
		}
		print '</div>';


		print '<div id="wrapper_'.$ddmmyyyy.'">';

		// hours
		// print 1 canvas chart for each status code
		$all_status_collector =[];
		foreach ($all_results[$ddmmyyyy]["hours"] as  $arr_status_code) {

			foreach ($arr_status_code as $status_code => $useless) {
				if(!array_key_exists($status_code, $all_status_collector)){
					$all_status_collector[$status_code] = 1;

					$id = $ddmmyyyy.'_'.$status_code;  // unique chart id

					print "<h2> status code $status_code</h2>";
					print '
					<div>
						<canvas id="'.$id.'" height="450" width="600"></canvas>
					</div>
					';
					// print "<h1> will build ".$id.'</h1>';  // DEV, quitar en prod
					print "<script>";
					print generateLineChart($all_results[$ddmmyyyy]["hours"],$id);

					// js paint chart
					// id is used for html canvas element AND for javascript var with chart Data
					// nan added so not starting with number
					print '
						var ctx = document.getElementById("'.$id.'").getContext("2d");
						window.myLine = new Chart(ctx).Line(nan_'.$id.', {
							responsive: true
						});
					';
					print "</script>";
				}
			}
		}


		print "</div><!-- close day wrapper -->"; 
		print "<hr>";
	}

}

function generateLineChart($arr_hours,$id){
	$return = "";
	$return .=  "var nan_".$id." = {";
	list($labels,$datasets) = genenerateLabelsAndDataset($arr_hours,$id);
	$return .= "labels : ".$labels.",";
	$return .= "datasets : ".$datasets;
	return $return."}";
}

// used for hours
function genenerateLabelsAndDataset($arr_hours,$id){
	$tmp_arr = explode('_', $id); // sacar status code del $id
	$status_code = end($tmp_arr);

	// generate Labels
	$labels = json_encode(array_keys($arr_hours));
	// populate with 0 if not defined
	$cero = (isset($arr_hours["00"][$status_code]))?$arr_hours["00"][$status_code]:"0";
	$uno = (isset($arr_hours["01"][$status_code]))?$arr_hours["01"][$status_code]:"0";
	$dos = (isset($arr_hours["02"][$status_code]))?$arr_hours["02"][$status_code]:"0";
	$tres = (isset($arr_hours["03"][$status_code]))?$arr_hours["03"][$status_code]:"0";
	$cuatro = (isset($arr_hours["04"][$status_code]))?$arr_hours["04"][$status_code]:"0";
	$cinco = (isset($arr_hours["05"][$status_code]))?$arr_hours["05"][$status_code]:"0";
	$seis = (isset($arr_hours["06"][$status_code]))?$arr_hours["06"][$status_code]:"0";
	$siete = (isset($arr_hours["07"][$status_code]))?$arr_hours["07"][$status_code]:"0";
	$ocho = (isset($arr_hours["08"][$status_code]))?$arr_hours["08"][$status_code]:"0";
	$nueve = (isset($arr_hours["09"][$status_code]))?$arr_hours["09"][$status_code]:"0";
	$diez = (isset($arr_hours["10"][$status_code]))?$arr_hours["10"][$status_code]:"0";
	$once = (isset($arr_hours["11"][$status_code]))?$arr_hours["11"][$status_code]:"0";
	$doce = (isset($arr_hours["12"][$status_code]))?$arr_hours["12"][$status_code]:"0";
	$trece = (isset($arr_hours["13"][$status_code]))?$arr_hours["13"][$status_code]:"0";
	$catorce = (isset($arr_hours["14"][$status_code]))?$arr_hours["14"][$status_code]:"0";
	$quince = (isset($arr_hours["15"][$status_code]))?$arr_hours["15"][$status_code]:"0";
	$diezyseis = (isset($arr_hours["16"][$status_code]))?$arr_hours["16"][$status_code]:"0";
	$diezysiete = (isset($arr_hours["17"][$status_code]))?$arr_hours["17"][$status_code]:"0";
	$diezyocho = (isset($arr_hours["18"][$status_code]))?$arr_hours["18"][$status_code]:"0";
	$diezynueve = (isset($arr_hours["19"][$status_code]))?$arr_hours["19"][$status_code]:"0";
	$veinte = (isset($arr_hours["20"][$status_code]))?$arr_hours["20"][$status_code]:"0";
	$ventiuno = (isset($arr_hours["21"][$status_code]))?$arr_hours["21"][$status_code]:"0";
	$ventidos = (isset($arr_hours["22"][$status_code]))?$arr_hours["22"][$status_code]:"0";
	$ventritres = (isset($arr_hours["23"][$status_code]))?$arr_hours["23"][$status_code]:"0";

	// generateDatasets
	$datasets = "[{";
	$datasets .= '  
					fillColor : "rgba(220,220,220,0.2)",
					strokeColor : "rgba(220,220,220,1)",
					pointColor : "rgba(220,220,220,1)",
					pointStrokeColor : "#fff",
					pointHighlightFill : "#fff",
					pointHighlightStroke : "rgba(220,220,220,1)",
					data : [
						"'.$cero.'",
						"'.$uno.'",
						"'.$dos.'",
						"'.$tres.'",
						"'.$cuatro.'",
						"'.$cinco.'",
						"'.$seis.'",
						"'.$siete.'",
						"'.$ocho.'",
						"'.$nueve.'",
						"'.$diez.'",
						"'.$once.'",
						"'.$doce.'",
						"'.$trece.'",
						"'.$catorce.'",
						"'.$quince.'",
						"'.$diezyseis.'",
						"'.$diezysiete.'",
						"'.$diezyocho.'",
						"'.$diezynueve.'",
						"'.$veinte.'",
						"'.$ventiuno.'",
						"'.$ventidos.'",
						"'.$ventritres.'",
						]
					';
	$datasets .= "}]";
	return [$labels,$datasets];
	// $labels = '['
	// return $labels,$datasets;

}

// genenerateLabelsAndDataset($all_results,$ddmmyyyy);

generateCharts($all_results);

print "</body>
</html>";

// Original example data
// print '
// 	<body>
// 		<div style="width:30%">
// 			<div>
// 				<canvas id="canvas" height="450" width="600"></canvas>
// 			</div>
// 		</div>
// 	<script>
// 		var randomScalingFactor = function(){ return Math.round(Math.random()*100)};
// 		var lineChartData = {
// 			labels : ["January","February","March","April","May","June","July"],
// 			datasets : [
// 				{
// 					label: "My First dataset",
// 					fillColor : "rgba(220,220,220,0.2)",
// 					strokeColor : "rgba(220,220,220,1)",
// 					pointColor : "rgba(220,220,220,1)",
// 					pointStrokeColor : "#fff",
// 					pointHighlightFill : "#fff",
// 					pointHighlightStroke : "rgba(220,220,220,1)",
// 					data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
// 				},
// 				{
// 					label: "My Second dataset",
// 					fillColor : "rgba(151,187,205,0.2)",
// 					strokeColor : "rgba(151,187,205,1)",
// 					pointColor : "rgba(151,187,205,1)",
// 					pointStrokeColor : "#fff",
// 					pointHighlightFill : "#fff",
// 					pointHighlightStroke : "rgba(151,187,205,1)",
// 					data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
// 				}
// 			]
// 		}
// 	window.onload = function(){
// 		var ctx = document.getElementById("canvas").getContext("2d");
// 		window.myLine = new Chart(ctx).Line(lineChartData);
// 	}
// 	</script>
// 	</body>
// </html>
// ';