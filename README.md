Parseador básico de apache access_log

### INSTALACIÓN ###
- bajar proyecto
`git clone https://bitbucket.org/danielsalgadop/log_analyzer.git`

### USAGE ###
- buscar tipos de https status por cada día
`php log_analyzer.php  /path/al/access_log;`
- buscar ip sospechosas
`php suspect_ip.php /path/al/access_log`
- buscar peticiones para una ip concreta
`php ip_petitions ip /path/al/access_log`
- total de peticiones por cada hora
`php apache_petitions_per_hour.php /path/al/access_log`
- total de peticiones por cada hora y status
`php apache_petitions_per_hour.php /path/al/access_log`
- generar gráfico con todos los calculates por día (necesita de vendor/Chart.min.js)
`php all.php /path/al/access_log > output.html`

### Proyectos parecidos ###
1. PERL 
- http://www.awstats.org/
- http://alvinalexander.com/blog/post/perl/free-perl-program-read-apache-access-log-file   # most-requested files
2. PHP
- https://github.com/kassner/log-parser


### EN UN FUTURO ###
- Para detectar las horas que ha estado apagado/encendido
pensar si buscar esto
[Thu Feb 25 09:45:07.596679 2016] [mpm_prefork:notice] [pid 3306] AH00170: caught SIGWINCH, shutting down gracefully
[Thu Feb 25 09:45:09.695762 2016] [core:notice] [pid 3412] SELinux policy enabled; httpd running as context system_u:system_r:httpd_t:s0
[Thu Feb 25 09:45:09.696304 2016] [suexec:notice] [pid 3412] AH01232: suEXEC mechanism enabled (wrapper: /usr/sbin/suexec)
[Thu Feb 25 09:45:09.712665 2016] [auth_digest:notice] [pid 3412] AH01757: generating secret for digest authentication .